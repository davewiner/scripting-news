## Scripting-News

I'm starting to use GitHub for work on my blog. Why not? It's got good communication and collaboration tools. Why not hook it up to a blog?

#### How to use this repo

Well, that's sort of a trick question. You need to read the blog, [scripting.com](http://scripting.com/). I'll post questions in the form of an Issue, and a discussion may or may not ensue.

You can also _watch_ the repo, so you'll be informed via email when something new shows up here. There's an icon at the top of each page that allows you to watch or unwatch.