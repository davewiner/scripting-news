## This is a new Readme for the Scripting News repo

I am using the new _English_ editor to work on this.

But it's missing some capabilities, that's why I'm not editing the actual readme file, yet.

Let's see if this works! :-)

#### Observations

Obviously I'm going to want an outliner. I'm using Medium-Editor to try to create something generic first. I wish everyone loved outliners as much as I do.

#### Todo list

#### Done list

*   The commit message is now settable through the UI.
*   A link from the editor to the page in the repo. Also known as the Eye icon.
*   Option to not show Markdown preview.
*   No more hard-coded constants on the server, they all come through from the editor, and ones that the user can reasonably set are in the settings dialog.
*   We have two headline sizes, h2 and h4.